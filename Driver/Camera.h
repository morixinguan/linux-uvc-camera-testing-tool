#ifndef __CAMERA_H
#define __CAMERA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>


//向驱动申请帧缓冲的个数
#define BUFF_COUNT 3
#define CLEAR(x) memset(&(x), 0, sizeof(x))

struct buffer 
{
	void   *start;
	size_t length;
};

struct UsbCameraInfo
{
    const char *DevName;
	int Width;
	int Height;
	int Format;
};

//打开摄像头
int UsbCameraOpen(struct UsbCameraInfo *Info);
//关闭摄像头
int UsbCameraClose(void);
//启动摄像头数据流
int UsbCameraStartCapture(void);
//停止摄像头数据流
int UsbCameraStopCapture(void);
//获取摄像头数据流
struct buffer UsbCameraGetRawData();
//设置亮度
int UsbCameraSetBrightness(int brightness);
//设置对比度
int UsbCameraSetContrast(int contrast);
//设置饱和度
int UsbCameraSetSaturation(int saturation);
//设置色调
int UsbCameraSetHue(int Hue);
//设置清晰度
int UsbCameraSetSharpness(int Sharpness);
//设置增益
int UsbCameraSetGain(int Gain);
//设置伽马
int UsbCameraSetGamma(int Gamma);
//设置曝光
int UsbCameraSetExposure(int Exposure);
//停止内存映射
int UsbCameraMunmap(void);

#ifdef __cplusplus
}
#endif
#endif //__CAMERA_H
