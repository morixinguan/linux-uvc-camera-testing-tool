﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QTime>
#include <QDebug>
#include <QTimer>
#include <QDateTime>
#include <QMainWindow>
#include "Driver/Camera.h"

#define DEBUG_ON
#define CAMERA_WIDTH 640
#define CAMERA_HEIGHT 480
#define NR(x) sizeof(x)/sizeof(x[0])

enum
{
    CAMERA_IDLE = 0,
    CAMEAR_STOP = 1,
    CAMERA_START = 2,
    CAMERA_STREAM = 3
};


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    static void *CameraThreadHandler(void *arg);
signals:
    void CameraIsStop();
    void sendRawData(unsigned char *);

private slots:
    void showImg(QPixmap &img);
    void on_CameraOp_released();
    void CameraIsStopHandler();
    void handleRawData(unsigned char *);
    void on_brightness_ctl_valueChanged(int value);
    void on_contrast_ctl_valueChanged(int value);
    void on_sharpness_ctl_valueChanged(int value);
    void on_saturation_ctl_valueChanged(int value);
    void on_hue_ctl_valueChanged(int value);

private:
    Ui::MainWindow *ui;
    pthread_t CameraThread;
    uint8_t CameraState = 0;
    struct UsbCameraInfo CameraInfo;
    uint8_t StateMachine = CAMERA_IDLE;
};
#endif // MAINWINDOW_H
